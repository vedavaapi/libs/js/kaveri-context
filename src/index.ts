import { request } from '@vedavaapi/web';

export type IAuthEvent = 'AUTHORIZED' | 'UNAUTHORIZED' | 'REFRESHED';

export interface IMessageData {
    type: IAuthEvent;
}

class KNotifier {
    public static EVENT_AUTHORIZED = 'AUTHORIZED';

    public static EVENT_UNAUTHORIZED = 'UNAUTHORIZED';

    public static EVENT_REFRESHED = 'REFRESHED';

    private readonly origin: string | null;

    public onauthorized?: (data: IMessageData) => any;

    public onunauthorized?: (data: IMessageData) => any;

    public onrefreshed?: (data: IMessageData) => any;

    constructor(origin: string | null) {
        this.origin = origin;
        if (!this.origin) {
            return;
        }
        this.setupListener();
    }

    private setupListener() {
        window.addEventListener('message', (event) => {
            if (event.origin !== this.origin) {
                return;
            }
            const handler = {
                [KNotifier.EVENT_AUTHORIZED]: this.onauthorized,
                [KNotifier.EVENT_UNAUTHORIZED]: this.onunauthorized,
                [KNotifier.EVENT_REFRESHED]: this.onrefreshed,
            }[event.data.type];
            if (handler) {
                handler(event.data);
            }
        }, false);
    }
}

export interface IUser {
    name ?: string;
    email ?: string;
    picture ?: string;
}

export interface ISession {
    accessToken?: string;
    issuedAt?: string;
    expiresIn?: string;
    user?: IUser;
}

export interface ISessionResponse {
    site: string;
    session: ISession;
}

export interface IVC {
    base: string;
    accessToken?: string;
}

export type ISessionChangeHandler = (messageData: IMessageData, session: ISession | null) => any;

export default class KContext {
    public static request = request;

    public url: string | null;

    public sessionURL: string | null;

    public iframeURL: string | null;

    public iframe!: HTMLIFrameElement | null;

    public site: string | null;

    public session: ISession | null;

    public vc: IVC | null;

    private notifier: KNotifier;

    /**
     * following are bindings, to which if we attached handlers, they will be fired.
     * it is simple in sense that, events cannot be subscribed with many handlers, but one
     * it is generallly better to encapsulate this, in high level pub sub patterns, like svelte store.
     */

    public onconfirmed?: () => any; // confirms app is authorized

    public onauthorized?: ISessionChangeHandler;

    public onunauthorized?: ISessionChangeHandler;

    public onrefreshed?: ISessionChangeHandler;

    public onsessionchange?: ISessionChangeHandler; // fires for all

    public onsyncerror?: (messageData: IMessageData, error: Error) => any;

    constructor({ url }: {
        url: string | null;
    }) {
        this.url = url ? url.replace(/\/$/, '') : null;
        this.sessionURL = url ? new URL('apps/session', `${this.url}/`).href : null;
        this.iframeURL = url ? new URL('apps/app-embed', `${this.url}/`).href : null;
        this.site = null;
        this.session = null;
        this.vc = null;
        this.notifier = new KNotifier(url ? new URL(url).origin : null);
        this.setupIframe();
        this.setupNotifier();
    }

    private setupIframe() {
        if (!this.iframeURL) {
            this.iframe = null;
            return;
        }
        this.iframe = document.createElement('iframe');
        this.iframe.src = this.iframeURL;
        this.iframe.style.width = '0px';
        this.iframe.style.height = '0px';
        this.iframe.style.display = 'none';

        const onKaveriMount = (event: MessageEvent) => {
            // eslint-disable-next-line no-console
            console.log('recieved ready message', event);
            if (event.origin !== new URL(this.url!).origin || event.data.type !== 'ready') {
                return;
            }
            window.removeEventListener('message', onKaveriMount);
            this.iframe!.contentWindow!.postMessage({ type: 'initRequest' }, new URL(this.url!).origin);
        };
        window.addEventListener('message', onKaveriMount);

        document.body.appendChild(this.iframe);
    }

    public static getInstance() {
        const queryParams = new URLSearchParams(document.location.search.substring(1));
        const url = queryParams.get('kaveri');
        return new KContext({ url });
    }

    private setSession(sessionResponse: ISessionResponse) {
        this.site = sessionResponse.site;
        this.session = sessionResponse.session;
        this.vc = { base: this.site };
        if (this.session && this.session.accessToken) this.vc.accessToken = this.session.accessToken;
    }

    private async getSession() {
        if (!this.sessionURL) {
            throw Error('kaveri url is not setted');
        }
        const resp = await request(this.sessionURL, {
            credentials: 'include',
        });
        this.setSession(resp.data);
    }

    public async confirmWithServer() {
        // confirms with server that, this app is authorized.. and also syncs session if confirmed
        await this.getSession();
        if (this.onconfirmed) this.onconfirmed();
    }

    private setupNotifier() {
        const syncSessionAndNotify = async (data: IMessageData, handler?: ISessionChangeHandler) => {
            try {
                await this.getSession();
            } catch (e) {
                if (this.onsyncerror) this.onsyncerror(data, e);
            }
            if (handler) handler(data, this.session);
            if (this.onsessionchange) this.onsessionchange(data, this.session);
        };

        this.notifier.onauthorized = (data) => syncSessionAndNotify(data, this.onauthorized);
        this.notifier.onunauthorized = (data) => {
            this.session = null;
            delete (this.vc as IVC).accessToken;
            if (this.onunauthorized) this.onunauthorized(data, this.session);
            if (this.onsessionchange) this.onsessionchange(data, this.session);
        };
        this.notifier.onrefreshed = (data) => syncSessionAndNotify(data, this.onrefreshed);
    }

    public requestUserAuthorization() {
        this.iframe!.contentWindow!.postMessage({ type: 'authorizationRequest' }, new URL(this.url!).origin);
    }

    public requestUserUnAuthorization() {
        this.iframe!.contentWindow!.postMessage({ type: 'unAuthorizationRequest' }, new URL(this.url!).origin);
    }
}
